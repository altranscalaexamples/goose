import sbt._
object Dependencies {

  private[this] lazy val versionScalaTest = "3.0.5"
  private lazy val testDependencies = Seq(
    "org.scalatest" %% "scalatest"  % versionScalaTest % Test,
    "org.mockito"   % "mockito-all" % "1.10.19"        % Test
  )

  val dependencies = testDependencies

}
