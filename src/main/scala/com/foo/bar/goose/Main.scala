package com.foo.bar.goose

import com.foo.bar.goose.game.{GooseGame, Table, _}
import Init._
import com.foo.bar.goose.domain.model.Player
import com.foo.bar.goose.repository.UserInteractionRepository

object Main {

  def main(args: Array[String]): Unit = {

    val maybeWinner = for {
      playerNumber <- detectPlayers
      players      <- addPlayers(playerNumber)
      game         = GooseGame(players, UserInteractionRepository())
    } yield play(game.run())(game.finish)(game)

    maybeWinner match {
      case Right(winner) => println(s"${winner} wins")
      case Left(ex)      => Console.err.println(ex.decodeError)
    }

  }

  def play(table: Table)(finish: Table => Either[Table, Player])(implicit game: Game): String =
    finish(table) match {
      case Left(p)  => p.printTable; play(game.run(p))(finish)
      case Right(t) => s"${t.id}:${t.name}"
    }

}
