package com.foo.bar.goose.domain

package object error {

  implicit class ErrorDecoder(e: CustomError) {

    def decodeError: String =
      e match {
        case e: InitError       => e.msg
        case e: RepositoryError => e.ex.fold()(_.printStackTrace()); e.msg
        case e: ServiceError    => e.ex.fold()(_.printStackTrace()); e.msg
      }

  }

}
