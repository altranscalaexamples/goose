package com.foo.bar.goose

import com.foo.bar.goose.domain.model.{BRIDGE, DEATH, MAZE, Position}

package object domain {

  implicit class PositionOps(p: Position) {

    def evaluate: Position =
      p.pos match {
        case BRIDGE.pos => println("You are on the BRIDGE"); Position(12)
        case MAZE.pos   => println("You are on the MAZE"); Position(39)
        case DEATH.pos  => println("Sorry you are dead"); Position(0)
        case _          => p
      }
  }

}
