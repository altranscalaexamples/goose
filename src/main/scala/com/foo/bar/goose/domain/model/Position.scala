package com.foo.bar.goose.domain.model

import scala.util.Random

case class Position(pos: Int) {
  def +(p: Position): Position = Position(p.pos + pos)
}

object Position {
  val rand = new Random()

  def roll(dice1: Int = rand.nextInt(6) + 1, dice2: Int = rand.nextInt(6) + 1): Position = {
    println(s"Ok, $dice1, $dice2")
    Position(dice1 + dice2)
  }
}
