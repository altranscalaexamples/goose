package com.foo.bar.goose.domain.error

trait CustomError

case class RepositoryError(msg: String, ex: Option[Throwable]) extends CustomError
case class ServiceError(msg: String, ex: Option[Throwable])    extends CustomError
case class InitError(msg: String)                              extends CustomError
