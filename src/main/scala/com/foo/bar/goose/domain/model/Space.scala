package com.foo.bar.goose.domain.model

sealed trait Space {
  val pos: Int
}
case object BRIDGE extends Space { val pos = 6  }
case object MAZE   extends Space { val pos = 42 }
case object DEATH  extends Space { val pos = 58 }
case object END    extends Space { val pos = 63 }
