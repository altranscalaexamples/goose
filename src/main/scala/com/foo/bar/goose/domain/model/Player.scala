package com.foo.bar.goose.domain.model

case class Player(id: Int, name: String)
