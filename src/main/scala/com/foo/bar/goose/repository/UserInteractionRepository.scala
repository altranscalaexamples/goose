package com.foo.bar.goose.repository

import com.foo.bar.goose.domain.error.{CustomError, RepositoryError}

class UserInteractionRepository extends Interaction {

  override def interactForRolls(name: String): Either[CustomError, Option[(Int, Int)]] = {
    println(s"${name} is playing, do you want to roll? (Y/N)")
    Console.in.readLine() match {
      case "Y" => Right(getUserRolls)
      case "N" => Right(None)
      case "Q" => throw new Exception("Unrecoverable error")
      case x   => Left(RepositoryError(s"Invalid input $x", None))
    }
  }

  private def getUserRolls: Option[(Int, Int)] = {
    println("Enter your rolls: (D1,D2)")
    val in = Console.in.readLine()
    in.matches("[1-6],[1-6]") match {
      case true  => Option(in.split(",").map(_.toInt)).map(ar => (ar(0), ar(1)))
      case false => println("Wrong, I'll roll for you"); None
    }
  }
}

object UserInteractionRepository {
  def apply(): UserInteractionRepository = new UserInteractionRepository()
}
