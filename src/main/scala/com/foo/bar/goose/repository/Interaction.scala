package com.foo.bar.goose.repository

import com.foo.bar.goose.domain.error.CustomError

trait Interaction {

  def interactForRolls(name: String): Either[CustomError, Option[(Int, Int)]]

}
