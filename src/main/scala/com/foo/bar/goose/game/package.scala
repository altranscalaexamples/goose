package com.foo.bar.goose

import com.foo.bar.goose.domain.model.{END, Player, Position}

package object game {

  type Table = List[(Player, Position)]
  object Table {
    def apply(list: List[(Player, Position)] = List.empty): Table = list
  }

  implicit class TableOps(t: Table) {

    def printTable                            = t.foreach(p => println(s"${p._1.name} is at position ${p._2.pos}"))
    def get(player: Player): Option[Position] = t.find(_._1.equals(player)).map(_._2)

  }

  implicit class PlayerOps(lst: List[(Player, Position)]) {

    def asTable = Table(lst)

  }

  implicit class GameOps(t: Table) {

    def finish: Either[Table, Player] =
      t.find(p => (p._2.pos >= END.pos)) match {
        case Some(p) => Right(p._1)
        case None    => Left(t)
      }
  }
}
