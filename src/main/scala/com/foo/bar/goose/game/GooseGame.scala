package com.foo.bar.goose.game

import com.foo.bar.goose.domain.model.{Player, Position}
import com.foo.bar.goose.repository.Interaction

import scala.util.{Failure, Success, Try}

class GooseGame(val players: List[Player], interaction: Interaction) extends Game {

  def run(table: Table): Table =
    Try(players.map(p => (p, computePosition(p.name, table.get(p)))).asTable) match {
      case Success(newTable)  => newTable
      case Failure(exception) => Console.err.println(s"Wrong computation: ${exception.getMessage}, rollback"); table
    }

  private def computePosition(name: String, currentPos: Option[Position]): Position = {
    (userRolls(name), currentPos) match {
      case (Some(position), Some(oldPosition)) => position + oldPosition
      case (None, Some(oldPosition))           => oldPosition + Position.roll()
      case (Some(position), None)              => position
      case (None, None)                        => Position.roll()
    }
  }.evaluate

  private def userRolls(name: String): Option[Position] = interaction.interactForRolls(name) match {
    case Right(opt) => opt.flatMap(validateRolls(_)).map { case (d1, d2) => Position.roll(d1, d2) }
    case Left(ex)   => Console.err.println(s"Wrong computation: ${ex.decodeError}"); Some(Position(0))
  }

  private def validateRolls(d: (Int, Int), validRange: Seq[Int] = 1 until 6): Option[(Int, Int)] =
    if ((validRange contains d._1) && (validRange contains d._2))
      Some(d)
    else
      None

}

object GooseGame {
  def apply(players: List[Player], interaction: Interaction): Game = new GooseGame(players, interaction)
}
