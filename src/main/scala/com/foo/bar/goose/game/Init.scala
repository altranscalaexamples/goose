package com.foo.bar.goose.game

import com.foo.bar.goose.domain.error.{CustomError, InitError}
import com.foo.bar.goose.domain.model.Player

trait Init {

  def detectPlayers: Either[CustomError, Int] = {
    println("How many player wants to play?")
    val numPlayers = Console.in.readLine()
    numPlayers.matches("[2-4]") match {
      case true  => Right(numPlayers.toInt)
      case false => Left(InitError("Invalid configuration"))
    }
  }

  def addPlayers(numPlayers: Int): Either[CustomError, List[Player]] = {
    val base: Either[CustomError, List[Player]] = Right(List.empty[Player])
    Range(0, numPlayers).foldLeft(base) { (ls, id) =>
      addPlayer(ls.right.get, id)
    }
  }

  private def addPlayer(lst: List[Player], id: Int): Either[CustomError, List[Player]] = {
    println("Add player name:")
    val player = Player(id, Console.in.readLine())
    lst.contains(player) match {
      case true  => Left(InitError("Player already exists"))
      case false => Right(lst :+ player)
    }
  }

}

object Init extends Init
