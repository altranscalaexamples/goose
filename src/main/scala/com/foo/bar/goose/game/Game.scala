package com.foo.bar.goose.game

import com.foo.bar.goose.domain.model.Player

trait Game {

  val players: List[Player]

  def run(table: Table = Table()): Table

  def finish(table: Table): Either[Table, Player] = table.finish

}
