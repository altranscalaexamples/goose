package com.foo.bar.goose.game

import com.foo.bar.goose.domain.error.{CustomError, RepositoryError}
import com.foo.bar.goose.domain.model.{Player, Position}
import com.foo.bar.goose.repository.Interaction
import org.mockito.{ArgumentCaptor, Mockito}
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}
import org.mockito.Matchers._
import org.mockito.Mockito._

class GooseGameSpec extends WordSpec with Matchers with MockitoSugar with BeforeAndAfterEach {

  val repositoryMock = mock[Interaction]

  "GooseGame" should {

    val players = List(Player(0, "foo"), Player(1, "bar"))
    val game    = GooseGame(players, repositoryMock)

    "be able to play a run with 2 players" in {
      val correctAnswer: Either[CustomError, Option[(Int, Int)]] = Right(Some((2, 2)))
      when(repositoryMock.interactForRolls(any())).thenReturn(correctAnswer)
      val user = ArgumentCaptor.forClass(classOf[String])
      val tb   = game.run()

      tb shouldBe Table(players.map(p => (p, Position(4))))
      verify(repositoryMock, times(2)).interactForRolls(user.capture())

    }

    "be able to play without interaction" in {
      val emptyAnswer: Either[CustomError, Option[(Int, Int)]] = Right(None)
      when(repositoryMock.interactForRolls(any())).thenReturn(emptyAnswer)
      val user = ArgumentCaptor.forClass(classOf[String])

      val tb = game.run()
      tb.map(_._2) shouldNot be(List(Position(0), Position(0)))

      verify(repositoryMock, times(2)).interactForRolls(user.capture())

    }

    "rollback if repository fails" in {
      val wrong: Either[CustomError, Option[(Int, Int)]] = Left(RepositoryError("Fail", None))
      when(repositoryMock.interactForRolls(any())).thenReturn(wrong)
      val user  = ArgumentCaptor.forClass(classOf[String])
      val table = players.map(p => (p, Position(12)))
      val newTb = game.run(table)

      newTb shouldBe table

      verify(repositoryMock, times(2)).interactForRolls(user.capture())

    }

    "rollback if repository throws exception" in {
      when(repositoryMock.interactForRolls(any())).thenThrow(new RuntimeException("Fail"))
      val user  = ArgumentCaptor.forClass(classOf[String])
      val table = players.map(p => (p, Position(12)))
      val newTb = game.run(table)

      newTb shouldBe table

      verify(repositoryMock, times(1)).interactForRolls(user.capture())

    }

    "should finish if the first player wins" in {
      val table                                                  = players.map(p => (p, Position(65)))
      val correctAnswer: Either[CustomError, Option[(Int, Int)]] = Right(Some((2, 2)))
      when(repositoryMock.interactForRolls(any())).thenReturn(correctAnswer)
      val newTb = game.run(table)
      game.finish(newTb).right.get shouldBe players.head
    }

    "go to the bridge space" in {
      val correctAnswer: Either[CustomError, Option[(Int, Int)]] = Right(Some((3, 3)))
      when(repositoryMock.interactForRolls(any())).thenReturn(correctAnswer)
      val user = ArgumentCaptor.forClass(classOf[String])
      val tb   = game.run()

      tb shouldBe Table(players.map(p => (p, Position(12))))
      verify(repositoryMock, times(2)).interactForRolls(user.capture())

    }

    "go to the maze space" in {
      val table                                                  = players.map(p => (p, Position(38)))
      val correctAnswer: Either[CustomError, Option[(Int, Int)]] = Right(Some((2, 2)))
      when(repositoryMock.interactForRolls(any())).thenReturn(correctAnswer)
      val user = ArgumentCaptor.forClass(classOf[String])
      val tb   = game.run(table)

      tb shouldBe Table(players.map(p => (p, Position(39))))
      verify(repositoryMock, times(2)).interactForRolls(user.capture())

    }

    "go to the death space" in {
      val table                                                  = players.map(p => (p, Position(54)))
      val correctAnswer: Either[CustomError, Option[(Int, Int)]] = Right(Some((2, 2)))
      when(repositoryMock.interactForRolls(any())).thenReturn(correctAnswer)
      val user = ArgumentCaptor.forClass(classOf[String])
      val tb   = game.run(table)

      tb shouldBe Table(players.map(p => (p, Position(0))))
      verify(repositoryMock, times(2)).interactForRolls(user.capture())

    }

  }

  override def beforeEach(): Unit =
    Mockito.reset(repositoryMock)

}
