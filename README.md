#Instruction of the Game of the Goose

- You can add from 2 to 4 players, the names should be different
- Each player have to rolls two dices or the system rolls it for him
- The game finishes when a player reach space 63
- At space 6 the player finds the BRIDGE, he jumps to space 12
- At space 42 the player finds the MAZE, he go back to space 39
- At space 58 the player dies, he go back to 0