import sbt.Keys._
import sbt._
import Settings._
import Dependencies._

scalacOptions += "-Ypartial-unification"
scalacOptions += "-language:higherKinds"

lazy val goose = (project in file("."))
  .configs(IntegrationTest)
  .settings(name := "goose")
  .settings(libraryDependencies ++= dependencies)
  .defaultSettings
  .enablePlugins(JavaAppPackaging, DockerPlugin, AshScriptPlugin)

inConfig(IntegrationTest)(org.scalafmt.sbt.ScalafmtPlugin.scalafmtConfigSettings)
